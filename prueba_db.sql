--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = off;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET escape_string_warning = off;

SET search_path = public, pg_catalog;

--
-- Name: secuencia_depto; Type: SEQUENCE; Schema: public; Owner: rafa
--

CREATE SEQUENCE secuencia_depto
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.secuencia_depto OWNER TO rafa;

--
-- Name: secuencia_depto; Type: SEQUENCE SET; Schema: public; Owner: rafa
--

SELECT pg_catalog.setval('secuencia_depto', 4, true);


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: departamento; Type: TABLE; Schema: public; Owner: rafa; Tablespace: 
--

CREATE TABLE departamento (
    id integer DEFAULT nextval('secuencia_depto'::regclass) NOT NULL,
    name character varying(200),
    fk_provincia integer
);


ALTER TABLE public.departamento OWNER TO rafa;

--
-- Name: secuencito; Type: SEQUENCE; Schema: public; Owner: rafa
--

CREATE SEQUENCE secuencito
    START WITH 1
    INCREMENT BY 1
    NO MAXVALUE
    NO MINVALUE
    CACHE 1;


ALTER TABLE public.secuencito OWNER TO rafa;

--
-- Name: secuencito; Type: SEQUENCE SET; Schema: public; Owner: rafa
--

SELECT pg_catalog.setval('secuencito', 3, true);


--
-- Name: provincia; Type: TABLE; Schema: public; Owner: rafa; Tablespace: 
--

CREATE TABLE provincia (
    id integer DEFAULT nextval('secuencito'::regclass) NOT NULL,
    name character varying(200)
);


ALTER TABLE public.provincia OWNER TO rafa;

--
-- Data for Name: departamento; Type: TABLE DATA; Schema: public; Owner: rafa
--

COPY departamento (id, name, fk_provincia) FROM stdin;
1	san blas	2
2	chilecito	2
3	famatina	2
4	san martin	2
\.


--
-- Data for Name: provincia; Type: TABLE DATA; Schema: public; Owner: rafa
--

COPY provincia (id, name) FROM stdin;
1	santa cruz
2	la rioja
3	esperanza
\.


--
-- Name: departamento_pkey; Type: CONSTRAINT; Schema: public; Owner: rafa; Tablespace: 
--

ALTER TABLE ONLY departamento
    ADD CONSTRAINT departamento_pkey PRIMARY KEY (id);


--
-- Name: provincia_pkey; Type: CONSTRAINT; Schema: public; Owner: rafa; Tablespace: 
--

ALTER TABLE ONLY provincia
    ADD CONSTRAINT provincia_pkey PRIMARY KEY (id);


--
-- Name: departamento_fk_provincia_fkey; Type: FK CONSTRAINT; Schema: public; Owner: rafa
--

ALTER TABLE ONLY departamento
    ADD CONSTRAINT departamento_fk_provincia_fkey FOREIGN KEY (fk_provincia) REFERENCES provincia(id);


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

